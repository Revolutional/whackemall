﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {
    public int score;
    public int lives;
    public Text liveText;
    public Text scoreText;
    public int numberOfHoles;
    public GameObject hole;

	// Use this for initialization
	void Start () 
    {
        for (int i = 0; i < numberOfHoles; i++)
        {
            Instantiate(hole, new Vector3(Random.Range(-5,8), Random.Range(-3,3), 0), Quaternion.identity);
        }
	}
	
	// Update is called once per frame
	void Update () 
    {
        scoreText.text = score.ToString();
        liveText.text = lives.ToString();
        win();
        lose();
	}

    void win()
    {
        if (score == 20)
        {
            SceneManager.LoadScene("WinScene");
        }
    }

    void lose()
    {
        if (lives <= 0)
        {
            SceneManager.LoadScene("LoseScene");
        }
    }
}
