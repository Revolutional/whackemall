﻿using UnityEngine;
using System.Collections;

public class moleScript : MonoBehaviour {

    Animator moleAnimation;
    bool hit;
    public float waitTime;
    public GameObject hole;
    public AudioSource moleSound;

	// Use this for initialization
	void Start () {
        moleAnimation = transform.GetComponent<Animator>();
        moleSound = transform.GetComponent<AudioSource>();
        StartCoroutine("Change");
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    void OnMouseDown()
    {
        if (hit == false)
        {
            AddScore();
            hit = true;
            if (moleSound.isPlaying == false)
            {
                moleSound.Play();
            }

            StopAllCoroutines();
            moleAnimation.SetBool("Continue", true);
            moleAnimation.SetBool("Hit", true);
        }
    }

    IEnumerator Change()
    {
        yield return new WaitForSeconds(waitTime);
        moleAnimation.SetBool("Continue", true);
        moleAnimation.SetBool("Hit", false);
    }

    public void MinusLive()
    {
        Camera.main.transform.GetComponent<Player>().lives -= 1;
        delete();
    }

    public void AddScore()
    {
        Camera.main.transform.GetComponent<Player>().score += 1;
    }

    public void delete()
    {
        hole.GetComponent<holeScript>().willRespawn = true;
        Destroy(gameObject);
    }
}
