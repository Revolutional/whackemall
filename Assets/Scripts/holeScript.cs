﻿using UnityEngine;
using System.Collections;

public class holeScript : MonoBehaviour {

    public float randomTimer;
    public GameObject mole;
    public bool willRespawn;

	// Use this for initialization
	void Start () {
        randomTimer = Random.Range(1,7);

        StartCoroutine("respawn");
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (willRespawn == true)
        {
            transform.GetComponent<Collider2D>().enabled = true;
            randomTimer = Random.Range(1,5);
            StartCoroutine("respawn");
            willRespawn = false;
        }
	}

    void OnMouseDown()
    {
        Camera.main.transform.GetComponent<Player>().lives -= 1;
    }

    IEnumerator respawn()
    {
        yield return new WaitForSeconds(randomTimer);
        transform.GetComponent<Collider2D>().enabled = false;
        GameObject obj = (GameObject)Instantiate(mole, transform.position - new Vector3(0,0.45f,0), Quaternion.identity);
        obj.GetComponent<moleScript>().waitTime = Random.Range(2,5);
        obj.GetComponent<moleScript>().hole = transform.gameObject;
    }
}
