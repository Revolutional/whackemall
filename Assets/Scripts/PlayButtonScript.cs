﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlayButtonScript : MonoBehaviour 
{
    public void gameMenu (string sceneToChangeTo)
    {
        SceneManager.LoadScene(sceneToChangeTo);
    }
}
